---
layout: default
title: Home
nav_order: 1
description: "The Amateur Radio Inclusivity Pledge is a tool for showing commitment to inclusion in the amateur radio community"
permalink: /
last_modified_date: "2023-01-13 14:10"
---

# The Amateur Radio Inclusivity Pledge
{: .fs-9 }

The *Amateur Radio Inclusivity Pledge* is both a newly forming entity that scores amateur radio organizations based on their visible commitment to diversity and inclusion in the hobby, and a tool for showing that commitment.
{: .fs-6 .fw-300 }

---

## The Pledge

We affirm and promote the participation of ALL licensed amateurs in our hobby and service who celebrate the diversity of race, color, sex, range of abilities, affectional or sexual orientation, gender identity or expression, fursona, age, national origin, marital status, socioeconomic status, and physical characteristics represented in our community, according to the 7 principals detailed below:

1. The inherent worth and dignity of every person
2. Justice, equity and compassion in human relations
3. Acceptance of one another and encouragement to growth both inside and outside of our hobby and service
4. A free and responsible search for truth and meaning
5. The right of conscience and the use of the democratic process within our community, and society as a whole
6. The goal of world community with peace, liberty, and justice for all
7. Respect for the interdependent web of existence of which we are a part  

[Make the Pledge](/docs/make-the-pledge/){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 }

---

### Suggestions for Edits to the Pledge?

Let us know by e-mailing us at [AmateurRadioInclusivityPledge@proton.me](mailto:amateurradioinclusivitypledge@proton.me).
