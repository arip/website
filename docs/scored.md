---
layout: default
title: Scored Parties
nav_order: 20
last_modified_date: "2023-01-13 14:10"
---

# Scored Parties
{: .no_toc }
We evaluated our first selection of organizations throughout the month of December 2022. We sent our results to the reviewed organizations during the last week of the December via the contact information available on their public websites, and we then gave a 2 week waiting period for those organizations to respond if they wished. Below are the results of this first round of evaluations.  

Feel free to recommend any organization for review and scoring in the future by sending an email to [AmateurRadioInclusivityPledge@proton.me](mailto:amateurradioinclusivitypledge@proton.me).

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
- TOC
{:toc}
</details>

# Reports
* [2022, December - Report and Letter From the Founder](/assets/Report202212.pdf)

# Organizations
Organizations are scored based on the [set of criteria available here](/docs/scoring/). If the organization has made the pledge, their pledge level will be included along with their score. 

## Gold Organizations
Organizations that have scored a minimum of 100 points on our audit. 
  
| Organization                                                 | Inclusivity Statement                   | Pledge Level | Score |
|:-------------------------------------------------------------|:----------------------------------------|:-------------|------:|
|[Pride Radio](https://prideradio.group/)                      |[Values](https://prideradio.group/values)|              |    119|
|[Handiham](https://handiham.org/)                             |[About Us](https://handiham.org/wordpress1/about-us/)|  |    116|
|[N3VEM Repeater](https://n3vem.com/repeater)                  |[Mission](https://n3vem.com/repeater)    |Gold          |    109|

## Silver Organizations
Organizations that have scored from 30 - 99 points on our audit.  

| Organization                                                 | Inclusivity Statement                   | Pledge Level | Score |
|:-------------------------------------------------------------|:----------------------------------------|:-------------|------:|
|[ARIP](https://amateurradioinclusivitypledge.org)             |[ARIP](https://amateurradioinclusivitypledge.org/)|Silver|    70|
|[Radio Club of America](https://www.radioclubofamerica.org/)  |NA                                       |              |     68|
|[Kenwood](https://www.kenwood.com/usa/)                       |[Diversity & Inclusion](https://www.jvckenwood.com/en/sustainability/social/diversity-inclusion.html)| | 64|
|[mastodon.hams.social](https://mastodon.hams.social)          |[About](https://mastodon.hams.social/about)|            |     57|
|[mastodon.radio](https://mastodon.radio)                      |[About](https://mastodon.radio/about)    |              |     57|
|[qth.social](https://qth.social)                              |[About](https://qth.social/about)        |              |     57|

## Bronze Organizations
Organizations that have scored from 1 - 29 points on our audit.  

| Organization                                                 | Inclusivity Statement                   | Pledge Level | Score |
|:-------------------------------------------------------------|:----------------------------------------|:-------------|------:|
|[RSGB](https://rsgb.org/main/)                                |[Equality and Diversity](https://rsgb.services/public/publications/policy/RSGB_equality_and_diversity_policy.pdf), [Code of Conduct](https://rsgb.org/main/about-us/rsgb-code-of-conduct/)| | 17|
|[AR Newsline](https://www.arnewsline.org/)                    |NA                                       |              |     13|
|[ARRL](http://arrl.org/)                                      |NA                                       |              |     13|
|[Lithuanian Amateur Radio Society](https://lrmd.lt/)          |NA                                       |              |     13|
|[Radio Amateurs of Canada](https://www.rac.ca/)               |NA                                       |              |     13|
|[AMPR](https://www.ampr.org/)[^1]                             |[Values](https://www.ampr.org/about/values/)|           |      6|
|[Belize Amateur Radio Club](https://barc.bz)                  |[Constitution](https://barc.bz)          |              |      4|
|[M17 Project](https://m17project.org/)                        |[Code of Conduct](https://m17project.org/code-of-conduct)||    4|
|[QRZ](https://www.qrz.com/)                                   |[Forum Code of Conduct](https://forums.qrz.com/index.php?pages/QRZ_Forums_Code_of_Conduct/)||2|
|[Irish Radio Transmitters Society](https://www.irts.ie/cgi/index.cgi)|[Mission Statement](https://www.irts.ie/cgi/index.cgi)||1|

## Failed Organizational Audits
Organizations that scored a 0 on our audit, or that engaged in discriminatory practices as verified by legal decision or settlement. Why put scores of 0 alongside organizations that actively discriminate? Simple - Those that are silent are guilty of complicity.  
  
| Organization                                                 | Inclusivity Statement                   | Pledge Level | Score |
|:-------------------------------------------------------------|:----------------------------------------|:-------------|------:|
|[Alinco](http://www.alinco.com/global.html)|NA||0|
|[Amateur Funk Vereins Liechtenstein](https://afvl.li/)|NA||0|
|[AMSAT](https://www.amsat.org/)|NA||0|
|[Aruba ARC](https://www.qsl.net/aarc/)|NA||0|
|[Brandmeister](https://brandmeister.network/)|NA||0|
|[Cayman Amateur Radio Society](http://caymanhams.org/)|NA||0|
|[Cyprus Amateur Radio Society](https://www.cyhams.org/wp/)|NA||0|
|[Czech Radio Club](http://www.crk.cz)|NA||0|
|[Emirates Amateur Radio Society](https://ears.ae/en/)|NA||0|
|[Estonian Radio Amateurs Union](https://www.erau.ee)|NA||0|
|[Flex Radio](https://www.flexradio.com/)|NA||0|
|[Freestar](https://freestar.network/)|NA||0|
|[Gibraltar Amateur Radio Society](http://www.gibradio.net)|NA||0|
|[Hungarian Radio Amateur Society](https://mrasz.hu/)|NA||0|
|[IARU](https://www.iaru.org/)|NA||0|
|[Icom](https://www.icomjapan.com)|NA||0|
|[Jamaica Amateur Radio Association](http://www.jamaicaham.org/)|NA||0|
|[Malaysian Amateur Radio Transmitters’ Society](http://marts.org.my/)|NA||0|
|[Malta Amateur Radio League](http://www.9h1mrl.org/)|NA||0|
|[Mauritius Amateur Radio Society](http://www.qsl.net/mars)|NA||0|
|[Namibian Amateur Radio League](http://www.narlnam.com/)|NA||0|
|[New Zealand Association of Radio Transmitters Inc.](https://www.nzart.org.nz/)|NA||0|
|[Pakistan Amateur Radio Society](http://pakhams.com/)|NA||0|
|[Phillipine Amateur Radio Association](https://www.para.org.ph/)|NA||0|
|[Radio Society of Bermuda](https://www.radiobda.com)|NA||0|
|[Radio Society of Kenya](https://www.rsk.or.ke/)|NA||0|
|[Radio Society of Sri Lanka](https://rssl.lk/)|NA||0|
|[Royal Belgian Amateur Radio Union](https://www.uba.be/)|NA||0|
|[Saudi Amateur Radio Society](https://sars.sa)|NA||0|
|[Seychelles Amateur Radio Association](https://www.sara.sc/)|NA||0|
|[Singapore Amateur Radio Transmitting Society](https://www.sarts.org.sg/)|NA||0|
|[Syrian Scientific Technical Amateur Radio Society]()|NA||0|
|[Tajik Amateur Radio League](https://www.qsl.net/tarl/general.html)|NA||0|
|[TGIF dmr](https://tgif.network/)|NA||0|
|[The YL System](https://ylsystem.org/)|NA||0|
|[Verona Radio Club Curaçao](https://www.verona-club.eu)|NA||0|
|[Wireless Institute of Australia](https://www.wia.org.au/)|NA||0|
|[Yeasu](https://www.yaesu.com/)|NA||0|

# Individuals
We only include individuals who have made the pledge, asked to be included, and received a passing score. Individual scores are not made public.  

| Individual                                                   | Inclusivity Statement                                          |
|:-------------------------------------------------------------|:---------------------------------------------------------------|
|[N3VEM](https://n3vem.com)                                    |[Inclusivity Statement](https://n3vem.com/blog/inclusivity/)    |
  
# Footnotes
    
[^1]: There is some publicly available commentary that gives the appearance that AMPR may have engaged in discriminatory practices in awarding grants. ARIP is not a legal entity and cannot provide judgement on these issues, other than to provide links to the publicly available commentary upon request.