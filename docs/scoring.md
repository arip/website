---
layout: default
title: Inclusivity Scoring
nav_order: 25
last_modified_date: "2023-01-13 14:10"
---

# Inclusivity Scoring

Scoring is done based on a prioritized list of items, and scored using the Fibonacci Sequence for weighting. Higher scores are better.

Please feel free to make suggestions for changes to this list by emailing [AmateurRadioInclusivityPledge@proton.me](mailto:amateurradioinclusivitypledge@proton.me).

|Points|Item|
|0|Meets none of the requirements|
|1|Has an inclusivity statement or non-discrimination statement|
|1|Has a statement, specifically identifying 3 or more disadvantaged groups|
|2|Has a statement, specifically identifying 7 or more disadvantaged groups|
|3|Has made the ARIP pledge|
|5|Endorses, supports, or funds a group that has diversity as a core part of its mission|
|8|Core mission involves/targets the advancement of 1 or more disadvantaged groups|
|13|Holds an annual, publicly visible event, that targets inclusivity|
|21|Holds a monthly, publicly visible event, that targets inclusivity|
|34|Holds a weekly, publicly visible event, that targets inclusivity|
|55|Holds a daily, publicly visible event, that targets inclusivity. Daily items can include 'always on' activities, i.e. running a repeater system, digital mode 'rooms', informational web sites or bulletins, etc.|