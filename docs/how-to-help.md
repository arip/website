---
layout: default
title: How to Help
nav_order: 30
last_modified_date: "2023-01-13 14:10"
---

# How to Help
{: .no_toc }

* Spread the word! Share this site with other hams, clubs, net managers, equipment suppliers, etc.
* Suggest organizations to review
* Volunteer!
    * Since we're just getting up and running, we don't know exactly what all of our needs will be, but we forsee a lot of excellent things that an organization like this could contribute to the hobby & service, beyond just a pledge. If you think you might be willing to pitch in as we get up and running, send your information to [AmateurRadioInclusivityPledge@proton.me](mailto:amateurradioinclusivitypledge@proton.me), and we'll keep in touch as we figure it out!

## Items or Services we Need
If you are willing to donate your time or services, check back often, as we'll add the things we need as we discover them.

* We need an awesome logo! If you're willing to create one that would be licensed as [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/), let us know! We may be able to pay a small fee for this service, depending on the artist and their rates.

* We need early adopters, who are willing to make a visible pledge to inclusivity - [Make the Pledge](/docs/make-the-pledge/)

* Bi-lingual reviewers - we would love to be able to evaluate non-english speaking organizations by having individuals review the content in the native language. We would hate to mis-categorize an organization by basing their review of of incomplete or incorrectly translated materials.

* We need officers! - one of the things we would like to do is organize a very basic club, and apply for a club callsign so that ARIP can apply for a club call, and do all of the exciting things that go along with being a club.