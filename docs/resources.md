---
layout: default
title: Resources
nav_order: 40
last_modified_date: "2023-01-03 14:15"
---

# Resources
{: .no_toc }

* ['Who We Are' Research and Paper by Michelle Thompson W5NYV](https://github.com/Abraxas3d/Demographics/blob/master/Who-We-Are.pdf)
    * [Repository of tools Michelle has shared for conducting her research](https://github.com/Abraxas3d/Demographics)
* [The Future of Ham Radio by Frank Howell K4FMH](https://www.youtube.com/watch?v=W4H6wWd3k5s)
* [The Secret Storm Approaching CW Contesting](https://www.amateurradio.com/the-secret-storm-approaching-cw-contesting/)
* [A study of Amateur Radio Gender Demographics](http://web.archive.org/web/20070223193600/http://www.arrl.org/news/features/2005/03/15/1/?nc=1)


