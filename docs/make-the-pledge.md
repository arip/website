---
layout: default
title: Make the Pledge
nav_order: 10
last_modified_date: "2023-01-13 14:10"
---

# Make the Pledge

We're just getting started, but if you're eager to make the pledge, or want to be an early adopter, here's what you need to do:

1. Be sure your website, blog, social media profile, or some other publicly visible location associated with you or your organization has either:
    * Your own inclusivity statement[^1]  
    or  
    * A link to our inclusivity statement, along with the text "Proudly Making the Amateur Radio Inclusivity Pledge[^2]"
        * If linking to our statement feel free to use this code:  
        ```
        <a href="www.amateurradioinclusivitypledge.org">Proudly Making the Amateur Radio Inclusivity Pledge </a>
        ```
2. Choose a pledge level (required for organizations, optional for individuals):
    * *Bronze* : Pledge to, at a minimum, maintain a public statement of inclusion
    * *Silver* : Pledge to maintain a minimum [inclusivity score](/docs/scoring) of 30 points annually.
    * *Gold* : Pledge to maintain a minium [inclusivity score](/docs/scoring) of 100 points annually.

3. Email links to your website and inclusivity pledge, and your chosen pledge level (if applicable) to [AmateurRadioInclusivityPledge@proton.me](mailto:amateurradioinclusivitypledge@proton.me).
    * We will perform or update our evaluation, send you the results, and add you or your organization to our list of [Scored Parties](/docs/scored/) if you were previously not included.

---
[^1]: We will periodically audit inclusivity statements of linked organizations. If we find they no longer align with our mission or values, we reserve the right to remove individuals or organizations from our pledges list without notice, but will make an attempt to contact the individual or organization to rectify any errors first.
[^2]: We reserve the right to make updates to this pledge at any time. If you choose to link to ours, please check back often to ensure that we have not made any changes to it that would no longer align with your values or those of your organization.
