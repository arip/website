---
layout: default
title: Social
nav_order: 45
last_modified_date: "2023-01-13 14:00"
---

# Social
{: .no_toc }

ARIP doesn't maintain a blog, but for updates on activity, conversations we're taking part in, etc. please follow us on mastodon, or check back here often to see what we're discussing in the community.

<iframe allowfullscreen sandbox="allow-top-navigation allow-scripts allow-popups allow-popups-to-escape-sandbox" width="600" height="1000" src="https://mastofeed.com/apiv2/feed?userurl=https%3A%2F%2Fmastodon.hams.social%2Fusers%2Famateurradioinclusivitypledge&theme=dark&size=100&header=true&replies=false&boosts=true"></iframe>